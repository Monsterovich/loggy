#ifndef JOURNALVIEWER_H
#define JOURNALVIEWER_H

#include "common.h"

class JournalViewer : public QThread
{
	Q_OBJECT
public:
	JournalViewer();
	~JournalViewer();

	bool open(const QString &fileName);
	void setPositionToEnd();
	void setPositionToStart();

	bool isEndReached() { return reachedEnd; }
	bool isStartReached() { return reachedStart; }

public slots:
	void readLines(bool scrollToEnd, quint32 maxRead = 0);
	void readLinesBackwards(bool scrollToEnd, quint32 maxRead = 0);
private slots:
	void fileChanged(const QString &path);
signals:
	void lineRead(const QString &content, bool backwards, bool scrollToEnd);
	void linesReadingFinished(bool backwards, bool scrollToEnd);
	void journalChanged();

private:
	QFile file;
	QFileSystemWatcher watcher;

	bool reachedEnd, reachedStart;
	qint64 lastForwardPosition, lastBackwardPosition;
};

#endif // JOURNALVIEWER_H
