#ifndef COMMON_H
#define COMMON_H

// Qt
#include <QObject>
#include <QApplication>
#include <QMainWindow>
#include <QThread>
#include <QFile>
#include <QFileSystemWatcher>
#include <QFileDialog>
#include <QDebug>
#include <QMessageBox>
#include <QScrollBar>
#include <QMetaType>
#include <QListWidgetItem>
#include <QKeyEvent>

#endif // COMMON_H
