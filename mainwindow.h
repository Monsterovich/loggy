#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "common.h"
#include "journalviewer.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
	Q_OBJECT
	
public:
	MainWindow(QWidget *parent = nullptr);
	~MainWindow();

private:
	void highlightMatchingItems(int start);
	void highlightMatchingItemsBackwards(int start);
	void clearItemsHighlighting();
	void searchForward();
	void searchBackward();

protected:
	void keyPressEvent(QKeyEvent *event);

private slots:
	void onLineRead(const QString &content, bool backwards, bool scrollToEnd);
	void onLinesReadingFinished(bool backwards, bool scrollToEnd);
	void onJournalChanged();

private slots:
	void on_actionOpen_log_triggered();
	void on_scrollBar_value_changed(int value);
	void on_actionScroll_to_end_triggered();
	void on_search_returnPressed();
	void on_journal_itemClicked(QListWidgetItem *item);
	void on_actionSearch_triggered();
	void on_closeTools_clicked();
	void on_searchBackward_clicked();
	void on_searchForward_clicked();

	void on_actionQuit_triggered();
	
private:
	Ui::MainWindow *ui;

	JournalViewer *viewer;
	QListWidgetItem *itemCursor; // this item is required for saving item position when scrolling to start

	quint32 linesRead;
	int searchPosition;
};
#endif // MAINWINDOW_H
