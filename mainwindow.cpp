#include "mainwindow.h"
#include "ui_mainwindow.h"

// fixme: hardcoded value
static const quint32 lines_read_max = 1024;

static const QColor highlighted_item_color = "yellow";

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
	ui->setupUi(this);
	connect(ui->journal->verticalScrollBar(), &QScrollBar::valueChanged, this, &MainWindow::on_scrollBar_value_changed);

	viewer = nullptr;
	itemCursor = nullptr;
	searchPosition = 0;

	// resolve performance issues with scrollToBottom when retrieving lines from end
	ui->journal->setUniformItemSizes(true);

	ui->tools->setVisible(false);
}

MainWindow::~MainWindow()
{
	delete ui;
}

void MainWindow::highlightMatchingItems(int start)
{
	for (int i = start; i < ui->journal->count(); i++) {
		QListWidgetItem *item = ui->journal->item(i);

		if (item->text().contains(ui->search->text(), 
								  ui->caseSensetive->isChecked() ? Qt::CaseSensitive : Qt::CaseInsensitive)) {
			item->setBackground(QBrush(highlighted_item_color));
		}
	}
}

void MainWindow::highlightMatchingItemsBackwards(int start)
{
	for (int i = start; i > 0; i--) {
		QListWidgetItem *item = ui->journal->item(i);

		if (item->text().contains(ui->search->text(),
								  ui->caseSensetive->isChecked() ? Qt::CaseSensitive : Qt::CaseInsensitive)) {
			item->setBackground(QBrush(highlighted_item_color));
		}
	}
}

void MainWindow::clearItemsHighlighting()
{
	for (int i = 0; i < ui->journal->count(); i++) {
		QListWidgetItem *item = ui->journal->item(i);
		item->setBackground(QApplication::palette("QListWidgetItem").base());
	}
}

void MainWindow::searchForward()
{
	for (int i = searchPosition + 1; i < ui->journal->count(); i++) {
		QListWidgetItem *item = ui->journal->item(i);

		if (item->text().contains(ui->search->text())) {
			ui->journal->scrollToItem(item);
			ui->journal->setCurrentItem(item);

			searchPosition = i;
			break;
		}
	}
}

void MainWindow::searchBackward()
{
	for (int i = searchPosition - 1; i > 0; i--) {
		QListWidgetItem *item = ui->journal->item(i);

		if (item->text().contains(ui->search->text())) {
			ui->journal->scrollToItem(item);
			ui->journal->setCurrentItem(item);

			searchPosition = i;
			break;
		}
	}
}

void MainWindow::keyPressEvent(QKeyEvent *event)
{
	if (event->key() == Qt::Key_Escape && ui->tools->isVisible()) {
		ui->tools->setVisible(false);
	}
}

/*
 * JournalViewer -> UI 
 * slots
*/

void MainWindow::onLineRead(const QString &content, bool backwards, bool scrollToEnd)
{
	linesRead++;
	ui->statusbar->showMessage(tr("Lines read: %1").arg(QString::number(linesRead)), INT_MAX);

	if (backwards) {
		ui->journal->insertItem(0, content);
	} else {
		ui->journal->addItem(content);
	}

	if (scrollToEnd) {
		ui->journal->scrollToBottom();
	}

	if (backwards && scrollToEnd) {
		searchPosition = ui->journal->count() - 1;
	}
}

void MainWindow::onLinesReadingFinished(bool backwards, bool scrollToEnd)
{
	if (backwards && !scrollToEnd && itemCursor) {
		ui->journal->scrollToItem(itemCursor);
	}

	if (!ui->search->text().isEmpty() && itemCursor) {
		if (backwards) {
			highlightMatchingItemsBackwards(ui->journal->row(itemCursor) - 1);
		} else {
			highlightMatchingItems(ui->journal->row(itemCursor) - 1);
		}
	}
}

void MainWindow::onJournalChanged()
{
	if (viewer->isEndReached()) {
		QMetaObject::invokeMethod(viewer, "readLines", Qt::QueuedConnection, 
								  Q_ARG(bool, ui->actionAuto_scroll->isChecked()));
	}
}

/*
 * UI slots 
*/

void MainWindow::on_actionOpen_log_triggered()
{
	const QStringList filters({"Log files (*.log)",
							   "Any files (*)"
							  });
	QFileDialog dialog(this);
	dialog.setNameFilters(filters);
	dialog.exec();

	if (!dialog.selectedFiles().empty()) {
		delete viewer;
		viewer = new JournalViewer;

		if (!viewer->open(dialog.selectedFiles().first())) {
			QMessageBox msgBox(this);
			msgBox.setWindowTitle(tr("Error"));
			msgBox.setText(tr("Couldn't open a file!"));
			msgBox.setStandardButtons(QMessageBox::Ok);
			msgBox.setIcon(QMessageBox::Critical);
			msgBox.exec();
			return;
		}

		connect(viewer, &JournalViewer::lineRead, this, &MainWindow::onLineRead);
		connect(viewer, &JournalViewer::linesReadingFinished, this, &MainWindow::onLinesReadingFinished);
		connect(viewer, &JournalViewer::journalChanged, this, &MainWindow::onJournalChanged);

		ui->journal->clear();
		ui->journal->scrollToTop();

		ui->actionScroll_to_end->setEnabled(true);

		linesRead = 0;

		viewer->setPositionToStart();

		QMetaObject::invokeMethod(viewer, "readLines", Qt::QueuedConnection, 
								  Q_ARG(bool, false),
								  Q_ARG(quint32, lines_read_max));
	}
}

void MainWindow::on_scrollBar_value_changed(int value)
{
	if (viewer->isEndReached() && !viewer->isStartReached()) {
		if (value == 0) {
			itemCursor = ui->journal->item(0);
			QMetaObject::invokeMethod(viewer, "readLinesBackwards", Qt::QueuedConnection, 
									  Q_ARG(bool, false),
									  Q_ARG(quint32, lines_read_max));
		}
	}

	if (viewer->isStartReached() && !viewer->isEndReached()) {
		if (value == ui->journal->verticalScrollBar()->maximum()) {
			itemCursor = ui->journal->item(ui->journal->count() - 1);
			QMetaObject::invokeMethod(viewer, "readLines", Qt::QueuedConnection, 
									  Q_ARG(bool, false),
									  Q_ARG(quint32, lines_read_max));
		}
	}
}

void MainWindow::on_actionScroll_to_end_triggered()
{
	if (viewer->isEndReached()) {
		ui->journal->scrollToBottom();
	} else {
		ui->journal->clear();
	
		linesRead = 0;

		viewer->setPositionToEnd();
		QMetaObject::invokeMethod(viewer, "readLinesBackwards", Qt::QueuedConnection, 
								  Q_ARG(bool, true),
								  Q_ARG(quint32, lines_read_max));
	}
}

void MainWindow::on_search_returnPressed()
{
	if (ui->search->text().isEmpty()) {
		clearItemsHighlighting();
		return;
	}

	clearItemsHighlighting();
	highlightMatchingItems(0);
	searchForward();
}

void MainWindow::on_journal_itemClicked(QListWidgetItem *item)
{
	searchPosition = ui->journal->row(item) - 1;
}

void MainWindow::on_actionSearch_triggered()
{
	ui->tools->setVisible(true);
	ui->search->setFocus();
}

void MainWindow::on_closeTools_clicked()
{
	ui->tools->setVisible(false);
}

void MainWindow::on_searchBackward_clicked()
{
	if (ui->search->text().isEmpty()) {
		clearItemsHighlighting();
		return;
	}

	clearItemsHighlighting();
	highlightMatchingItemsBackwards(ui->journal->count() - 1);
	searchBackward();
}

void MainWindow::on_searchForward_clicked()
{
	on_search_returnPressed();
}

void MainWindow::on_actionQuit_triggered()
{
	close();
}
