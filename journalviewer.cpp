#include "journalviewer.h"

JournalViewer::JournalViewer()
{
	reachedEnd = false;
	reachedStart = false;

	// move slots and objects to thread
	moveToThread(this);
	file.moveToThread(this);
	watcher.moveToThread(this);

	connect(&watcher, &QFileSystemWatcher::fileChanged, this, &JournalViewer::fileChanged);

	// start thread
	start();
}

JournalViewer::~JournalViewer()
{
	quit();

	if (!wait()) {
		terminate();
		wait();
	}
}

bool JournalViewer::open(const QString &filePath)
{
	file.setFileName(filePath);

	bool result1 = file.open(QFile::ReadOnly | QFile::Text);
	bool result2 = watcher.addPath(filePath);

	return result1 && result2;
}

void JournalViewer::setPositionToEnd()
{
	lastForwardPosition = file.size();
	lastBackwardPosition = file.size();

	reachedEnd = false;
	reachedStart = false;
}

void JournalViewer::setPositionToStart()
{
	lastForwardPosition = 0;
	lastBackwardPosition = 0;

	reachedEnd = false;
	reachedStart = false;
}

void JournalViewer::readLines(bool scrollToEnd, quint32 maxRead)
{
	quint32 linesRead = 0;

	file.seek(lastForwardPosition);

	while (!file.atEnd()) {
		if (linesRead >= maxRead && maxRead > 0) {
			lastForwardPosition = file.pos();
			reachedStart = true;
			emit linesReadingFinished(false, scrollToEnd);
			return;
		}

		QByteArray line = file.readLine();
		linesRead++;

		// remove unnecessary newlines
		if (line[line.length() - 1] == '\n') {
			line.remove(line.length() - 1, 1);
		}

		emit lineRead(QString::fromUtf8(line), false, scrollToEnd);
	}

	lastForwardPosition = file.pos();
	reachedEnd = true;
	emit linesReadingFinished(false, scrollToEnd);
}

void JournalViewer::readLinesBackwards(bool scrollToEnd, quint32 maxRead)
{
	quint32 linesRead = 0;

	file.seek(lastBackwardPosition);

	while (file.pos() > 1) {
		if (linesRead >= maxRead && maxRead > 0) {
			lastBackwardPosition = file.pos();
			reachedEnd = true;
			emit linesReadingFinished(true, scrollToEnd);
			return;
		}

		QByteArray line;
		linesRead++;

		// read line by each character
		while (file.pos() > 1) {
			file.seek(file.pos() - 2);

			char c;
			file.read(&c, sizeof(char));

			if (c == '\n') {
				break;
			}

			line.prepend(c);
		}

		emit lineRead(QString::fromUtf8(line), true, scrollToEnd);
	}

	lastBackwardPosition = file.pos();
	reachedStart = true;
	emit linesReadingFinished(true, scrollToEnd);
}

void JournalViewer::fileChanged(const QString &path)
{
	Q_UNUSED(path)

	emit journalChanged();
}

